import java.util.PriorityQueue;

public class Main {

    public static void main(String[] args) {
        final PriorityQueue<VerbalNumber> numbers = new PriorityQueue<>((n1, n2) -> n1.getNumber()-n2.getNumber());

        Thread readerThread = new Thread(new ReaderThread(numbers));
        Thread writerThread = new Thread(new WriterThread(numbers));

        readerThread.start();
        writerThread.start();
    }

}
