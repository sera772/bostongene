import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class VerbalNumber {

    @Getter
    private String verbalNumber;
    @Getter
    private int number;
    private static Map<String, Integer> wordNumber;

    public static VerbalNumber createInstance(String request) throws ParserException {
        if (request == null || request.length() < 3) {
            throw new ParserException("Incorrect input");
        }
        int number = parseWords(request.trim().split(" "));
        return new VerbalNumber(request, number);
    }

    private static int parseWords(String[] words) throws ParserException {
        int result = 0, dig = 0;
        for (int i = 0; i < words.length; ++i) {
            Integer val = wordNumber.get(words[i].toLowerCase());
            if (val == null) {
                throw new ParserException("Incorrect input: " + words[i]);
            }
            if (isDigit(val)) {
                if (dig != 0) {
                    throw new ParserException("Incorrect input: " + words[i-1] + " " + words[i]);
                }
                dig = val;
            }
            else if(isDec(val)) {
                if (dig != 0) {
                    throw new ParserException("Incorrect input: " + words[i-1] + " " + words[i]);
                }
                result += val;
            }
            else{
                if (dig == 0) {
                    throw new ParserException("Incorrect input: " + words[i-1] + " " + words[i]);
                }
                result += dig*val;
                dig = 0;
            }
        }
        result += dig;

        if (result < 1 || result > 9999) {
            throw new ParserException("Incorrect input");
        }

        return result;
    }

    private static boolean isDigit(Integer number) {
        return number < 10;
    }

    private static boolean isDec(Integer number) {
        return number < 100;
    }

    static {
        wordNumber = new HashMap<String, Integer>() {
            {
//**********Numbers from 1 to 19
                put("one",        1);
                put("two",        2);
                put("three",      3);
                put("four",       4);
                put("five",       5);
                put("six",        6);
                put("seven",      7);
                put("eight",      8);
                put("nine",       9);
                put("ten",        10);
                put("eleven",     11);
                put("twelve",     12);
                put("thirteen",   13);
                put("fourteen",   14);
                put("fifteen",    15);
                put("sixteen",    16);
                put("seventeen",  17);
                put("eighteen",   18);
                put("nineteen",   19);
//**********Decades, hundred, thousand
                put("twenty",     20);
                put("thirty",     30);
                put("forty",      40);
                put("fifty",      50);
                put("sixty",      60);
                put("seventy",    70);
                put("eighty",     80);
                put("ninety",     90);
                put("hundred",   100);
                put("thousand", 1000);
            }
        };
    }

////////////////////////////////////////////////////////////

    public static class ParserException extends Exception {
        ParserException(String message) {
            super(message);
        }
    }

}
