import lombok.AllArgsConstructor;

import java.util.PriorityQueue;

@AllArgsConstructor
public class WriterThread implements Runnable {

    private final PriorityQueue<VerbalNumber> numbers;

    @Override
    public void run() {
        while (true) {
            try{
                Thread.sleep(5000);
                synchronized (numbers) {
                    if (numbers.isEmpty()) {
                        System.out.println("WriterThread: Nothing to show");
                    }
                    else {
                        System.out.println(numbers.poll().getVerbalNumber());
                    }
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
