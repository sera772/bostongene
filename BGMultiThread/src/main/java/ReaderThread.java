import lombok.AllArgsConstructor;

import java.util.PriorityQueue;
import java.util.Scanner;

@AllArgsConstructor
public class ReaderThread implements Runnable {

    private final PriorityQueue<VerbalNumber> numbers;

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while(true){
            String request = scanner.nextLine();
            try{
                VerbalNumber verbalNumber = VerbalNumber.createInstance(request);
                synchronized (numbers){
                    numbers.offer(verbalNumber);
                }
            }
            catch (VerbalNumber.ParserException e){
                System.out.println(e.getMessage());
            }
        }
    }
}
