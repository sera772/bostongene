package web.model.repository;

import org.springframework.data.repository.CrudRepository;
import web.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail( String email);
}
