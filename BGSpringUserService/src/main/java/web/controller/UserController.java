package web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import web.model.User;
import web.model.repository.UserRepository;

import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/all")
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public User findUserById(@PathVariable long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent())
            throw new NoSuchElementException();
        return user.get();
    }

    @GetMapping("/findByEmail/{email}")
    public User findByEmail(@PathVariable String email) {
        User user = userRepository.findByEmail(email);
        Iterable<User> users = userRepository.findAll();
        if (user == null){
            throw new NoSuchUserException();
        }
        return user;
    }

    @PostMapping("")
    public User createUser(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable long id) {
        userRepository.deleteById(id);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class NoSuchUserException extends RuntimeException {

        NoSuchUserException() {
            super("No such user");
        }
    }

}
