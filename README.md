Тестовые задания для BostonGene
---

## Задача 1: Многопоточность

Сборка и запуск:

- cd BGMultiThread
- mvn compile
- mvn exec:java -Dexec.mainClass="Main"

---

## Задача 2: 

Сборка и запуск:

- cd BGSpringUserService
- mvn clean install
- cd target
- java -jar BGSpringUserService-1.0.jar

Примеры запросов:

1) Вывести всех ползователей на экран:
	
	curl --location --request GET "localhost:8080/users/all"
2) Найти пользователя по id:

	curl --location --request GET "localhost:8080/users/{id}"
3) Найти пользователя по email:

	curl --location --request GET "localhost:8080/users/findByEmail/{email}"
4) Удалить пользователя по id:

	curl --location --request DELETE "localhost:8080/users/{id}"
5) Добавить пользователя (JSON; необходимые поля: firstName, lastName, email, password, birthday):
	
	curl --location --request POST "localhost:8080/users" \
	--header "Content-Type: application/json" \
	--data "{
	\"firstName\": \"Sergey\",
	\"lastName\": \"Radchenko\",
	\"email\": \"sera772@gmail.com\",
	\"password\": \"sOmEpAsSwOrD\",
	\"birthday\": \"1995-07-03\"
	}"

---

## Задача 3: Yandex translate API

Сборка и запуск:

- cd BGYaTranslator
- mvn compile
- mvn exec:java -Dexec.mainClass="Main"