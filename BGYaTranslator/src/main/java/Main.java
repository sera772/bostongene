import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("Enter phrase: ");
            String phrase = scanner.nextLine();
            System.out.println(Translator.translatePhrase(phrase));
            System.out.print("If you wish to continue, type yes: ");
            if (!scanner.nextLine().toLowerCase().equals("yes")){
                System.out.println("Thank you");
                return;
            }
        }
    }
}
