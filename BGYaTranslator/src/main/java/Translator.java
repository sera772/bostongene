import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;

public class Translator {
    private static String URL_KEY = "https://translate.yandex.net/api/v1.5/tr.json/translate" +
            "?key=trnsl.1.1.20190607T205405Z.e379d7e498e022f0.4ddd47770df501796fb9d89279242adc671a77e1" +
            "&lang=en-ru" +
            "&text=";

    public static String translatePhrase(String phrase) {
        try {
            URL url = new URL(URL_KEY + URLEncoder.encode(phrase, "UTF-8"));
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            try (DataOutputStream dos = new DataOutputStream(connection.getOutputStream())) {
                dos.flush();
            }
            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                StringBuilder response = new StringBuilder();
                String s;
                while ((s = br.readLine()) != null) {
                    response.append(s);
                    response.append("\n");
                }
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject.getInt("code") == 200) {
                    return jsonObject.getJSONArray("text").getString(0);
                }
                return "Bad request: " + jsonObject.getInt("code");
            }
            finally {
                connection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
